<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            ["name" => "Test User1","acc_number" => "2562342120001","password" => Hash::make("6968"),"point"=>2250.00],
            ["name" => "Test User2","acc_number" => "2562342120002","password" => Hash::make("4116"),"point"=>6650.00],
            ["name" => "Test User2","acc_number" => "2562342120003","password" => Hash::make("8142"),"point"=>470.00],
        ]);
    }
}
