@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>Welcome {{ Auth::user()->name }}</h1>
                    <h2>Your point is {{ Auth::user()->point }}</h2>
                    <hr>
                    <h4>Deposit points</h4>
                    <p>
                        <a href="/deposit/20" class="btn btn-primary">20 แต้ม</a>
                        <a href="/deposit/150" class="btn btn-primary">150 แต้ม</a>
                        <a href="/deposit/300" class="btn btn-primary">300 แต้ม</a>
                        <a href="/deposit/500" class="btn btn-primary">500 แต้ม</a>
                    </p>
                    <hr>
                    <h4>Withdraw points</h4>
                    <p>
                        <a href="/withdraw/20" class="btn btn-primary">20 แต้ม</a>
                        <a href="/withdraw/150" class="btn btn-primary">150 แต้ม</a>
                        <a href="/withdraw/300" class="btn btn-primary">300 แต้ม</a>
                        <a href="/withdraw/500" class="btn btn-primary">500 แต้ม</a>
                    </p>
                    <h4>Transfer Point</h4>
                    <form action="/transfer" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                              <div class="form-group col-sm-5">
                                <label for="acc_number">Account Number</label>
                                <input type="text" class="form-control" name="acc_number" id="acc_number" placeholder="Enter Account Number">
                              </div>
                              <div class="form-group  col-sm-5">
                                <label for="amount">Amount</label>
                                <input type="number" class="form-control" name="amount" id="amount" placeholder="Enter Amount">
                              </div>
                              <div class="form-group  col-sm-2">
                              <button type="submit" class="btn btn-primary">Transfer</button>
                              </div>
                        </div>
                    </form>
                    <table class="table table-bordered">
                        <tr>
                            <th>Timestamp</th>
                            <th>Action</th>
                        </tr>
                        @forelse($logs as $log)
                            <tr>
                                <td>
                                    {{$log->created_at}}
                                </td>
                                <td>
                                @if($log->action == "deposit")
                                    Deposit +{{$log->amount}} point.
                                @elseif($log->action == "withdraw")
                                    Withdraw -{{$log->amount}} point.
                                @elseif($log->action == "transfer")
                                    Transfer to {{$log->receiver_acc}} amount -{{$log->amount}} point.
                                @elseif($log->action == "receive")
                                    Receive from {{$log->receiver_acc}} amount +{{$log->amount}} point.
                                @endif
                                </td>
                            </tr>
                        @empty
                        <tr>
                            <td colspan="3" align="center">
                                No transaction
                            </td>
                        </tr>
                        @endforelse

                        @if($logs->lastPage() > 1)
                        <tr>
                            <td colspan="3">
                               {{ $logs->links() }}
                            </td>
                        </tr>
                        @endif
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
